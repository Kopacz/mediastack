import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mediastack/app/app.dart';
import 'package:mediastack/app/config.dart';
import 'package:mediastack/app/flavor.dart';
import 'package:mediastack/app/service_locator.dart';
import 'package:mediastack/features/intro/cubit/intro_cubit.dart';

Future<void> startApp({required Flavor flavor}) async {
  Config.flavor = flavor;
  WidgetsFlutterBinding.ensureInitialized();
  setupServiceLocator();

  return runApp(
    BlocProvider(
      lazy: false,
      create: (context) => serviceLocator.get<IntroCubit>()..onInit(),
      child: const App(),
    ),
  );
}
