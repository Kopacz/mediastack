import 'package:mediastack/app/flavor.dart';
import 'package:mediastack/main_common.dart';

void main() {
  startApp(flavor: Prod());
}
