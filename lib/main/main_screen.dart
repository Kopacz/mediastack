import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mediastack/app/service_locator.dart';
import 'package:mediastack/features/home/cubit/home.dart';
import 'package:mediastack/features/home/cubit/home_cubit.dart';
import 'package:mediastack/main/main_bottom_navigation_bar.dart';

class MainScreen extends StatefulWidget {
  const MainScreen({super.key, this.initialTabIndex = 0});

  final int initialTabIndex;

  @override
  State<MainScreen> createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  int tabIndex = 0;

  @override
  void initState() {
    super.initState();
    tabIndex = widget.initialTabIndex;
  }

  void _onChanged(int selectedPageIndex) {
    setState(() {
      tabIndex = selectedPageIndex;
    });
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<HomeCubit>(
      create: (context) => serviceLocator.get()..onInit(),
      child: Scaffold(
        body: [
          const Home(),
          const Center(
            child: Text('FAVOURITES'),
          )
        ].elementAt(tabIndex),
        bottomNavigationBar: MainBottomNavigationBar(
          onTabChanged: _onChanged,
          selectedTabIndex: tabIndex,
        ),
      ),
    );
  }
}
