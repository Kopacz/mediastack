import 'package:flutter/material.dart';
import 'package:mediastack/main/main_bottom_navigation_bar_item.dart';
import 'package:mediastack/resources/assets.dart';
import 'package:mediastack/resources/dimensions.dart';

class MainBottomNavigationBar extends StatelessWidget {
  const MainBottomNavigationBar({
    required this.onTabChanged,
    required this.selectedTabIndex,
  });

  final ValueChanged<int> onTabChanged;
  final int selectedTabIndex;

  void _onItemTapped(NavBarTab newSelectedNavBarPage) => onTabChanged(newSelectedNavBarPage.index);

  @override
  Widget build(BuildContext context) {
    return Material(
      elevation: Dimensions.bottomNavBarElevation8,
      child: Container(
        color: Theme.of(context).bottomNavigationBarTheme.backgroundColor,
        child: SafeArea(
          child: SizedBox(
            height: Dimensions.bottomNavBarHeight,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                MainBottomNavigationBarItem(
                  unselectedIcon: Assets.iconHome,
                  selectedIcon: Assets.iconHomeFilled,
                  selected: NavBarTab.values[selectedTabIndex] == NavBarTab.home,
                  onTap: () => _onItemTapped(NavBarTab.home),
                ),
                MainBottomNavigationBarItem(
                  unselectedIcon: Assets.iconFavourites,
                  selectedIcon: Assets.iconFavouritesFilled,
                  selected: NavBarTab.values[selectedTabIndex] == NavBarTab.favourites,
                  onTap: () => _onItemTapped(NavBarTab.favourites),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

enum NavBarTab {
  home,
  favourites,
}
