import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mediastack/resources/dimensions.dart';
import 'package:mediastack/resources/palette.dart';
import 'package:mediastack/widgets/tap_area.dart';

class MainBottomNavigationBarItem extends StatelessWidget {
  const MainBottomNavigationBarItem({
    required this.unselectedIcon,
    required this.selectedIcon,
    required this.selected,
    required this.onTap,
    super.key,
  });

  final String unselectedIcon;
  final String selectedIcon;
  final bool selected;
  final VoidCallback onTap;

  @override
  Widget build(BuildContext context) {
    return Flexible(
      child: TapArea(
        borderRadius:
            BorderRadius.circular(Dimensions.bottomNavBarItemBorderRadius76),
        onTap: onTap,
        child: SizedBox(
          height: Dimensions.bottomNavBarItemHeight46,
          width: Dimensions.bottomNavBarItemWidth72,
          child: Stack(
            alignment: Alignment.center,
            children: [
              Container(
                height: Dimensions.bottomNavBarItemHeight46,
                width: Dimensions.bottomNavBarItemWidth72,
                decoration: ShapeDecoration(
                  color: selected
                      ? Palette.light.secondaryContainer
                      : Palette.light.background,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(
                      Dimensions.bottomNavBarItemBorderRadius76,
                    ),
                  ),
                ),
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SvgPicture.asset(
                    selected ? selectedIcon : unselectedIcon,
                    width: Dimensions.iconSize36,
                    height: Dimensions.iconSize36,
                    color: selected
                        ? Palette.light.primary
                        : Palette.light.onBackground,
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
