// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'intro_cubit.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$IntroState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() userNew,
    required TResult Function() userOnboarded,
    required TResult Function() error,
    required TResult Function() loading,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? userNew,
    TResult? Function()? userOnboarded,
    TResult? Function()? error,
    TResult? Function()? loading,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? userNew,
    TResult Function()? userOnboarded,
    TResult Function()? error,
    TResult Function()? loading,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_UserNew value) userNew,
    required TResult Function(_UserOnboarded value) userOnboarded,
    required TResult Function(_Error value) error,
    required TResult Function(_Loading value) loading,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_UserNew value)? userNew,
    TResult? Function(_UserOnboarded value)? userOnboarded,
    TResult? Function(_Error value)? error,
    TResult? Function(_Loading value)? loading,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_UserNew value)? userNew,
    TResult Function(_UserOnboarded value)? userOnboarded,
    TResult Function(_Error value)? error,
    TResult Function(_Loading value)? loading,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $IntroStateCopyWith<$Res> {
  factory $IntroStateCopyWith(
          IntroState value, $Res Function(IntroState) then) =
      _$IntroStateCopyWithImpl<$Res, IntroState>;
}

/// @nodoc
class _$IntroStateCopyWithImpl<$Res, $Val extends IntroState>
    implements $IntroStateCopyWith<$Res> {
  _$IntroStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$_UserNewCopyWith<$Res> {
  factory _$$_UserNewCopyWith(
          _$_UserNew value, $Res Function(_$_UserNew) then) =
      __$$_UserNewCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_UserNewCopyWithImpl<$Res>
    extends _$IntroStateCopyWithImpl<$Res, _$_UserNew>
    implements _$$_UserNewCopyWith<$Res> {
  __$$_UserNewCopyWithImpl(_$_UserNew _value, $Res Function(_$_UserNew) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_UserNew implements _UserNew {
  const _$_UserNew();

  @override
  String toString() {
    return 'IntroState.userNew()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_UserNew);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() userNew,
    required TResult Function() userOnboarded,
    required TResult Function() error,
    required TResult Function() loading,
  }) {
    return userNew();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? userNew,
    TResult? Function()? userOnboarded,
    TResult? Function()? error,
    TResult? Function()? loading,
  }) {
    return userNew?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? userNew,
    TResult Function()? userOnboarded,
    TResult Function()? error,
    TResult Function()? loading,
    required TResult orElse(),
  }) {
    if (userNew != null) {
      return userNew();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_UserNew value) userNew,
    required TResult Function(_UserOnboarded value) userOnboarded,
    required TResult Function(_Error value) error,
    required TResult Function(_Loading value) loading,
  }) {
    return userNew(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_UserNew value)? userNew,
    TResult? Function(_UserOnboarded value)? userOnboarded,
    TResult? Function(_Error value)? error,
    TResult? Function(_Loading value)? loading,
  }) {
    return userNew?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_UserNew value)? userNew,
    TResult Function(_UserOnboarded value)? userOnboarded,
    TResult Function(_Error value)? error,
    TResult Function(_Loading value)? loading,
    required TResult orElse(),
  }) {
    if (userNew != null) {
      return userNew(this);
    }
    return orElse();
  }
}

abstract class _UserNew implements IntroState {
  const factory _UserNew() = _$_UserNew;
}

/// @nodoc
abstract class _$$_UserOnboardedCopyWith<$Res> {
  factory _$$_UserOnboardedCopyWith(
          _$_UserOnboarded value, $Res Function(_$_UserOnboarded) then) =
      __$$_UserOnboardedCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_UserOnboardedCopyWithImpl<$Res>
    extends _$IntroStateCopyWithImpl<$Res, _$_UserOnboarded>
    implements _$$_UserOnboardedCopyWith<$Res> {
  __$$_UserOnboardedCopyWithImpl(
      _$_UserOnboarded _value, $Res Function(_$_UserOnboarded) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_UserOnboarded implements _UserOnboarded {
  const _$_UserOnboarded();

  @override
  String toString() {
    return 'IntroState.userOnboarded()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_UserOnboarded);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() userNew,
    required TResult Function() userOnboarded,
    required TResult Function() error,
    required TResult Function() loading,
  }) {
    return userOnboarded();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? userNew,
    TResult? Function()? userOnboarded,
    TResult? Function()? error,
    TResult? Function()? loading,
  }) {
    return userOnboarded?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? userNew,
    TResult Function()? userOnboarded,
    TResult Function()? error,
    TResult Function()? loading,
    required TResult orElse(),
  }) {
    if (userOnboarded != null) {
      return userOnboarded();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_UserNew value) userNew,
    required TResult Function(_UserOnboarded value) userOnboarded,
    required TResult Function(_Error value) error,
    required TResult Function(_Loading value) loading,
  }) {
    return userOnboarded(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_UserNew value)? userNew,
    TResult? Function(_UserOnboarded value)? userOnboarded,
    TResult? Function(_Error value)? error,
    TResult? Function(_Loading value)? loading,
  }) {
    return userOnboarded?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_UserNew value)? userNew,
    TResult Function(_UserOnboarded value)? userOnboarded,
    TResult Function(_Error value)? error,
    TResult Function(_Loading value)? loading,
    required TResult orElse(),
  }) {
    if (userOnboarded != null) {
      return userOnboarded(this);
    }
    return orElse();
  }
}

abstract class _UserOnboarded implements IntroState {
  const factory _UserOnboarded() = _$_UserOnboarded;
}

/// @nodoc
abstract class _$$_ErrorCopyWith<$Res> {
  factory _$$_ErrorCopyWith(_$_Error value, $Res Function(_$_Error) then) =
      __$$_ErrorCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_ErrorCopyWithImpl<$Res>
    extends _$IntroStateCopyWithImpl<$Res, _$_Error>
    implements _$$_ErrorCopyWith<$Res> {
  __$$_ErrorCopyWithImpl(_$_Error _value, $Res Function(_$_Error) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_Error implements _Error {
  const _$_Error();

  @override
  String toString() {
    return 'IntroState.error()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_Error);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() userNew,
    required TResult Function() userOnboarded,
    required TResult Function() error,
    required TResult Function() loading,
  }) {
    return error();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? userNew,
    TResult? Function()? userOnboarded,
    TResult? Function()? error,
    TResult? Function()? loading,
  }) {
    return error?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? userNew,
    TResult Function()? userOnboarded,
    TResult Function()? error,
    TResult Function()? loading,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_UserNew value) userNew,
    required TResult Function(_UserOnboarded value) userOnboarded,
    required TResult Function(_Error value) error,
    required TResult Function(_Loading value) loading,
  }) {
    return error(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_UserNew value)? userNew,
    TResult? Function(_UserOnboarded value)? userOnboarded,
    TResult? Function(_Error value)? error,
    TResult? Function(_Loading value)? loading,
  }) {
    return error?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_UserNew value)? userNew,
    TResult Function(_UserOnboarded value)? userOnboarded,
    TResult Function(_Error value)? error,
    TResult Function(_Loading value)? loading,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(this);
    }
    return orElse();
  }
}

abstract class _Error implements IntroState {
  const factory _Error() = _$_Error;
}

/// @nodoc
abstract class _$$_LoadingCopyWith<$Res> {
  factory _$$_LoadingCopyWith(
          _$_Loading value, $Res Function(_$_Loading) then) =
      __$$_LoadingCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_LoadingCopyWithImpl<$Res>
    extends _$IntroStateCopyWithImpl<$Res, _$_Loading>
    implements _$$_LoadingCopyWith<$Res> {
  __$$_LoadingCopyWithImpl(_$_Loading _value, $Res Function(_$_Loading) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_Loading implements _Loading {
  const _$_Loading();

  @override
  String toString() {
    return 'IntroState.loading()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_Loading);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() userNew,
    required TResult Function() userOnboarded,
    required TResult Function() error,
    required TResult Function() loading,
  }) {
    return loading();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? userNew,
    TResult? Function()? userOnboarded,
    TResult? Function()? error,
    TResult? Function()? loading,
  }) {
    return loading?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? userNew,
    TResult Function()? userOnboarded,
    TResult Function()? error,
    TResult Function()? loading,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_UserNew value) userNew,
    required TResult Function(_UserOnboarded value) userOnboarded,
    required TResult Function(_Error value) error,
    required TResult Function(_Loading value) loading,
  }) {
    return loading(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_UserNew value)? userNew,
    TResult? Function(_UserOnboarded value)? userOnboarded,
    TResult? Function(_Error value)? error,
    TResult? Function(_Loading value)? loading,
  }) {
    return loading?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_UserNew value)? userNew,
    TResult Function(_UserOnboarded value)? userOnboarded,
    TResult Function(_Error value)? error,
    TResult Function(_Loading value)? loading,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading(this);
    }
    return orElse();
  }
}

abstract class _Loading implements IntroState {
  const factory _Loading() = _$_Loading;
}
