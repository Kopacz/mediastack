part of 'intro_cubit.dart';

@freezed
class IntroState with _$IntroState {
  const factory IntroState.userNew() = _UserNew;

  const factory IntroState.userOnboarded() = _UserOnboarded;

  const factory IntroState.error() = _Error;

  const factory IntroState.loading() = _Loading;
}
