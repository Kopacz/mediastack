import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:mediastack/data/intro/intro_repository.dart';

part 'intro_state.dart';
part 'intro_cubit.freezed.dart';

class IntroCubit extends Cubit<IntroState> {
  IntroCubit(this._repository) : super(const IntroState.loading());

  final IntroRepository _repository;

  Future<void> onInit() async {
    await _checkIsOnboarded();
  }

  Future<void> _checkIsOnboarded() async {
    try {
      emit(const IntroState.loading());
      final isOnboarded = await _repository.isOnboarded();
      if (isOnboarded) {
        emit(const IntroState.userOnboarded());
      } else {
        emit(const IntroState.userNew());
      }
    } on Exception catch (_) {
      return emit(const IntroState.error());
    }
  }
}
