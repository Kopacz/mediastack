import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:mediastack/data/posts/model/post_dto.dart';
import 'package:mediastack/data/posts/posts_repository.dart';

part 'home_cubit.freezed.dart';
part 'home_state.dart';

class HomeCubit extends Cubit<HomeState> {
  HomeCubit(this._postsRepository) : super(const HomeState.loading());

  final PostsRepository _postsRepository;

  Future<void> onInit() async {
    emit(const HomeState.loading());

    final postsResult = await _postsRepository.getPosts();

    // final postsResult = await Result<PostsDto>.failure(DefaultError());

    // final postsResult = await Result.success(
    //   const PostsDto(
    //     data: [
    //       PostDto(
    //         author: 'Author',
    //         title: 'Title',
    //         description: 'Description',
    //         url: 'url',
    //         source: 'source',
    //         image: 'image',
    //         category: 'GENERAL',
    //         language: 'language',
    //         country: 'country',
    //       ),
    //     ],
    //   ),
    // );

    postsResult.when(
      success: (posts) => emit(HomeState.success(posts.data)),
      failure: (exception) => emit(const HomeState.error()),
    );
  }
}
