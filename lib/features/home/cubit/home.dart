import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mediastack/data/posts/model/post_dto.dart';
import 'package:mediastack/features/home/cubit/home_cubit.dart';
import 'package:mediastack/resources/assets.dart';
import 'package:mediastack/resources/dimensions.dart';
import 'package:mediastack/resources/labels.dart';
import 'package:mediastack/resources/palette.dart';
import 'package:mediastack/widgets/error_view.dart';
import 'package:mediastack/widgets/loader.dart';

class Home extends StatelessWidget {
  const Home();

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomeCubit, HomeState>(
      builder: (context, state) {
        return state.when(
          loading: () => const Center(
            child: Loader(),
          ),
          error: () => const Center(
            child: ErrorView(),
          ),
          success: (posts) => Scaffold(
            appBar: AppBar(
              title: const Text(Labels.homeTitleLabel),
              actions: const [
                _AppBarIconButton(iconAsset: Assets.iconSearch),
                _AppBarIconButton(iconAsset: Assets.iconCategories),
              ],
            ),
            body: ListView(
              children: List.generate(
                posts.length,
                (index) => _PostTile(post: posts[index]),
              ),
            ),
          ),
        );
      },
    );
  }
}

class _AppBarIconButton extends StatelessWidget {
  const _AppBarIconButton({required this.iconAsset});

  final String iconAsset;

  @override
  Widget build(BuildContext context) {
    return IconButton(
      icon: SvgPicture.asset(
        iconAsset,
        color: Theme.of(context).colorScheme.onBackground,
        width: Dimensions.iconSize20,
        height: Dimensions.iconSize20,
      ),
      onPressed: () {},
    );
  }
}

class _PostTile extends StatelessWidget {
  const _PostTile({required this.post});

  final PostDto post;

  String _formatCategoryText(String? category) {
    if (category == null) {
      return Labels.homeDefaultCategoryLabel;
    } else {
      return category.trim().toUpperCase();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(Dimensions.spacing12),
      child: DecoratedBox(
        decoration: BoxDecoration(
          color: Palette.light.secondaryContainer,
          borderRadius: BorderRadius.circular(Dimensions.radius16),
        ),
        child: Padding(
          padding: const EdgeInsets.fromLTRB(
            Dimensions.spacing24,
            Dimensions.spacing24,
            Dimensions.spacing24,
            Dimensions.spacing12,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Text(
                _formatCategoryText(post.category),
                style: Theme.of(context).textTheme.displaySmall?.copyWith(
                      color: Theme.of(context).colorScheme.onSecondaryContainer,
                    ),
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
              ),
              const SizedBox(height: Dimensions.spacing16),
              Text(
                post.title,
                style: Theme.of(context).textTheme.headlineLarge?.copyWith(
                      color: Theme.of(context).colorScheme.onSecondaryContainer,
                    ),
              ),
              const SizedBox(height: Dimensions.spacing12),
              Text(
                post.description,
                style: Theme.of(context).textTheme.headlineSmall?.copyWith(
                      color: Theme.of(context).colorScheme.onSecondaryContainer,
                    ),
                textAlign: TextAlign.justify,
                maxLines: 5,
                overflow: TextOverflow.ellipsis,
              ),
              const SizedBox(height: Dimensions.spacing12),
              Text(
                post.author != null ? post.author! : '',
                textAlign: TextAlign.end,
                style: Theme.of(context).textTheme.labelSmall?.copyWith(
                      color: Theme.of(context)
                          .colorScheme
                          .onSecondaryContainer
                          .withOpacity(0.5),
                    ),
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
