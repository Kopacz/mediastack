part of 'home_cubit.dart';

@freezed
class HomeState with _$HomeState {
  const factory HomeState.success(List<PostDto> posts) = _Success;

  const factory HomeState.error() = _Error;

  const factory HomeState.loading() = _Loading;
}
