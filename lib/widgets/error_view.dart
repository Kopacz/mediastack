import 'package:flutter/material.dart';
import 'package:mediastack/resources/assets.dart';
import 'package:mediastack/resources/dimensions.dart';
import 'package:mediastack/resources/labels.dart';

class ErrorView extends StatelessWidget {
  const ErrorView({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(Dimensions.spacing24),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset(Assets.imageError, height: 120),
          const SizedBox(height: Dimensions.spacing16),
          Text(
            Labels.errorLabel,
            style: Theme.of(context).textTheme.titleMedium?.copyWith(
              color: Theme.of(context).colorScheme.onSecondaryContainer,
            ),
          ),
        ],
      ),
    );
  }
}
