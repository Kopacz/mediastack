import 'package:flutter/material.dart';

class TapArea extends StatelessWidget {
  const TapArea({
    required this.onTap,
    required this.child,
    this.borderRadius,
    this.shape,
    super.key,
  });

  final VoidCallback? onTap;
  final Widget child;
  final BorderRadius? borderRadius;
  final ShapeBorder? shape;

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      borderRadius: borderRadius,
      shape: shape,
      child: SizedBox(
        child: InkWell(
          borderRadius: borderRadius,
          onTap: onTap,
          customBorder: shape,
          child: child,
        ),
      ),
    );
  }
}
