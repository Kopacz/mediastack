import 'package:flutter/material.dart';
import 'package:mediastack/resources/dimensions.dart';

class Loader extends StatelessWidget {
  const Loader({super.key});

  @override
  Widget build(BuildContext context) {
    return CircularProgressIndicator(
      color: Theme.of(context).colorScheme.onSecondaryContainer,
      strokeWidth: Dimensions.strokeWidth2,
    );
  }
}
