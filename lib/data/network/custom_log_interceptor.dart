import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';

class CustomLogInterceptor extends Interceptor {
  @override
  void onRequest(RequestOptions options, RequestInterceptorHandler handler) {
    debugPrint('HTTP REQUEST----------');
    debugPrint('Path: ${options.uri}');
    debugPrint('Method: ${options.method}');
    debugPrint('Headers: ${options.headers}');
    debugPrint('Parameters: ${options.data}');
    debugPrint('----------END OF REQUEST');
    return super.onRequest(options, handler);
  }

  @override
  void onResponse(Response<dynamic> response, ResponseInterceptorHandler handler) {
    debugPrint('HTTP RESPONSE----------');
    debugPrint('Path: ${response.realUri}');
    debugPrint('Status code: ${response.statusCode}');
    debugPrint('Status message: ${response.statusMessage}');
    debugPrint('Data: ${response.data}');
    debugPrint('----------END OF RESPONSE');
    return super.onResponse(response, handler);
  }

  @override
  void onError(DioError err, ErrorInterceptorHandler handler) {
    debugPrint('HTTP ERROR----------');
    debugPrint('Path: ${err.response?.requestOptions.path}');
    debugPrint('Error code: ${err.response?.statusCode}');
    debugPrint('Error message: ${err.response?.statusMessage}');
    debugPrint('----------END OF ERROR');
    return super.onError(err, handler);
  }
}
