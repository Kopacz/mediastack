import 'package:dio/dio.dart';
import 'package:mediastack/app/config.dart';

class AuthInterceptor extends Interceptor {
  AuthInterceptor();

  @override
  void onRequest(
    RequestOptions options,
    RequestInterceptorHandler handler,
  ) {
    final accessKey = Config.flavor.apiAccessKey;

    options.queryParameters.addAll(<String, String>{
      'access_key': accessKey,
    });

    return super.onRequest(options, handler);
  }
}
