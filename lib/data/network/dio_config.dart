import 'package:dio/dio.dart';
import 'package:mediastack/app/config.dart';
import 'package:mediastack/data/network/auth_interceptor.dart';
import 'package:mediastack/data/network/custom_log_interceptor.dart';

class DioConfig {
  static Dio createInstance(
    AuthInterceptor authInterceptor,
    CustomLogInterceptor logInterceptor,
  ) {
    final dio = Dio();

    dio.options.baseUrl = Config.flavor.apiBaseUrl;
    dio.interceptors.addAll([
      authInterceptor,
      logInterceptor,
    ]);

    return dio;
  }
}
