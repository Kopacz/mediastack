import 'package:dio/dio.dart';
import 'package:mediastack/data/common/mediastack_error.dart';
import 'package:mediastack/data/common/result.dart';
import 'package:mediastack/data/network/endpoints.dart';
import 'package:mediastack/data/posts/model/posts_dto.dart';

class PostsRestClient {
  PostsRestClient(this._dio);

  final Dio _dio;

  Future<Result<PostsDto>> getPosts() async {
    try {
      final response = await _dio.get<Map<String, Object?>>(Endpoints.news);

      if (response.data != null) {
        return Result.success(PostsDto.fromJson(response.data!));
      } else {
        return Result.failure(EmptyDataError());
      }
    } on DioError catch (_) {
      return Result.failure(ApiError());
    } catch (_) {
      return Result.failure(DefaultError());
    }
  }
}
