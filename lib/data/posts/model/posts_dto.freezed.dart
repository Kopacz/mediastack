// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'posts_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

PostsDto _$PostsDtoFromJson(Map<String, dynamic> json) {
  return _PostsDto.fromJson(json);
}

/// @nodoc
mixin _$PostsDto {
  List<PostDto> get data => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $PostsDtoCopyWith<PostsDto> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PostsDtoCopyWith<$Res> {
  factory $PostsDtoCopyWith(PostsDto value, $Res Function(PostsDto) then) =
      _$PostsDtoCopyWithImpl<$Res, PostsDto>;
  @useResult
  $Res call({List<PostDto> data});
}

/// @nodoc
class _$PostsDtoCopyWithImpl<$Res, $Val extends PostsDto>
    implements $PostsDtoCopyWith<$Res> {
  _$PostsDtoCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? data = null,
  }) {
    return _then(_value.copyWith(
      data: null == data
          ? _value.data
          : data // ignore: cast_nullable_to_non_nullable
              as List<PostDto>,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_PostsDtoCopyWith<$Res> implements $PostsDtoCopyWith<$Res> {
  factory _$$_PostsDtoCopyWith(
          _$_PostsDto value, $Res Function(_$_PostsDto) then) =
      __$$_PostsDtoCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({List<PostDto> data});
}

/// @nodoc
class __$$_PostsDtoCopyWithImpl<$Res>
    extends _$PostsDtoCopyWithImpl<$Res, _$_PostsDto>
    implements _$$_PostsDtoCopyWith<$Res> {
  __$$_PostsDtoCopyWithImpl(
      _$_PostsDto _value, $Res Function(_$_PostsDto) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? data = null,
  }) {
    return _then(_$_PostsDto(
      data: null == data
          ? _value._data
          : data // ignore: cast_nullable_to_non_nullable
              as List<PostDto>,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_PostsDto implements _PostsDto {
  const _$_PostsDto({required final List<PostDto> data}) : _data = data;

  factory _$_PostsDto.fromJson(Map<String, dynamic> json) =>
      _$$_PostsDtoFromJson(json);

  final List<PostDto> _data;
  @override
  List<PostDto> get data {
    if (_data is EqualUnmodifiableListView) return _data;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_data);
  }

  @override
  String toString() {
    return 'PostsDto(data: $data)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_PostsDto &&
            const DeepCollectionEquality().equals(other._data, _data));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(_data));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_PostsDtoCopyWith<_$_PostsDto> get copyWith =>
      __$$_PostsDtoCopyWithImpl<_$_PostsDto>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_PostsDtoToJson(
      this,
    );
  }
}

abstract class _PostsDto implements PostsDto {
  const factory _PostsDto({required final List<PostDto> data}) = _$_PostsDto;

  factory _PostsDto.fromJson(Map<String, dynamic> json) = _$_PostsDto.fromJson;

  @override
  List<PostDto> get data;
  @override
  @JsonKey(ignore: true)
  _$$_PostsDtoCopyWith<_$_PostsDto> get copyWith =>
      throw _privateConstructorUsedError;
}
