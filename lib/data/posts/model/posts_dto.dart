import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:mediastack/data/posts/model/post_dto.dart';

part 'posts_dto.freezed.dart';
part 'posts_dto.g.dart';

@freezed
class PostsDto with _$PostsDto {

  const factory PostsDto({
    required List<PostDto> data,
  }) = _PostsDto;

  factory PostsDto.fromJson(Map<String, Object?> json) => _$PostsDtoFromJson(json);
}
