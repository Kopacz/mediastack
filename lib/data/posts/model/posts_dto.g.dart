// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'posts_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_PostsDto _$$_PostsDtoFromJson(Map<String, dynamic> json) => _$_PostsDto(
      data: (json['data'] as List<dynamic>)
          .map((e) => PostDto.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$$_PostsDtoToJson(_$_PostsDto instance) =>
    <String, dynamic>{
      'data': instance.data,
    };
