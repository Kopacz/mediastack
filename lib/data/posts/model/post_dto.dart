import 'package:freezed_annotation/freezed_annotation.dart';

part 'post_dto.freezed.dart';
part 'post_dto.g.dart';

@freezed
class PostDto with _$PostDto {

  const factory PostDto({
    required String? author,
    required String title,
    required String description,
    required String? url,
    required String? source,
    required String? image,
    required String? category,
    required String? language,
    required String? country,
  }) = _PostDto;

  factory PostDto.fromJson(Map<String, Object?> json) => _$PostDtoFromJson(json);
}
