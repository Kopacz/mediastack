import 'package:mediastack/data/common/result.dart';
import 'package:mediastack/data/posts/model/posts_dto.dart';
import 'package:mediastack/data/posts/posts_rest_client.dart';

class PostsRepository {
  PostsRepository(this._restClient);

  final PostsRestClient _restClient;

  Future<Result<PostsDto>> getPosts() {
    return _restClient.getPosts();
  }
}
