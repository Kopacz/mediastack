import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:mediastack/data/common/mediastack_error.dart';

part 'result.freezed.dart';

@freezed
class Result<T> with _$Result<T> {
  Result._();

  factory Result.success(T result) = Success<T>;

  factory Result.failure(MediastackError error) = Failure<T>;
}
