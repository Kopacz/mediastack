abstract class MediastackError implements Exception {
  String get message;
}

class DefaultError extends MediastackError {
  DefaultError();

  @override
  String message = 'Mediastack error.';
}

class EmptyDataError extends MediastackError {
  EmptyDataError();

  @override
  String message = 'Request was successful but response data is null.';
}

class ApiError extends MediastackError {
  ApiError();

  @override
  String message = 'Request is unsuccessful due to API error.';
}
