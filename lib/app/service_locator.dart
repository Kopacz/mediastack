import 'package:get_it/get_it.dart';
import 'package:mediastack/data/intro/intro_repository.dart';
import 'package:mediastack/data/network/auth_interceptor.dart';
import 'package:mediastack/data/network/custom_log_interceptor.dart';
import 'package:mediastack/data/network/dio_config.dart';
import 'package:mediastack/data/posts/posts_repository.dart';
import 'package:mediastack/data/posts/posts_rest_client.dart';
import 'package:mediastack/features/home/cubit/home_cubit.dart';
import 'package:mediastack/features/intro/cubit/intro_cubit.dart';

final serviceLocator = GetIt.instance;

void setupServiceLocator() {
  _setupLibraries();
  _setupData();
  _setupCubits();
}

void _setupLibraries() {}

void _setupData() {
  serviceLocator
    ..registerFactory(AuthInterceptor.new)
    ..registerFactory(CustomLogInterceptor.new)
    ..registerLazySingleton(
      () => DioConfig.createInstance(
        serviceLocator.get(),
        serviceLocator.get(),
      ),
    )
    ..registerFactory(() => PostsRestClient(serviceLocator.get()))
    ..registerFactory(IntroRepository.new)
    ..registerFactory(() => PostsRepository(serviceLocator.get()));
}

void _setupCubits() {
  serviceLocator
    ..registerFactory(() => IntroCubit(serviceLocator.get()))
    ..registerFactory(() => HomeCubit(serviceLocator.get()));
}
