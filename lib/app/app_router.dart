import 'package:flutter/material.dart';
import 'package:mediastack/features/onboarding/onboarding_screen.dart';
import 'package:mediastack/main/main_screen.dart';

class AppRouter {
  AppRouter._();

  static const main = 'main';
  static const onboarding = 'onboarding';

  static PageRoute<Object?> onGenerateRoute(RouteSettings settings) {
    final uri = settings.name != null ? Uri.parse(settings.name!) : null;
    final name = uri?.pathSegments[0];
    Widget page;
    switch (name) {
      case AppRouter.onboarding:
        page = const OnboardingScreen();
        break;
      case AppRouter.main:
        page = const MainScreen();
        break;
      default:
        page = const MainScreen();
        break;
    }

    return MaterialPageRoute<Object?>(
      builder: (context) => page,
      settings: settings,
    );
  }
}
