import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mediastack/app/app_router.dart';
import 'package:mediastack/app/config.dart';
import 'package:mediastack/features/intro/cubit/intro_cubit.dart';
import 'package:mediastack/resources/themes.dart';

class App extends StatelessWidget {
  const App({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: Config.flavor.title,
      theme: Themes().appTheme,
      onGenerateRoute: AppRouter.onGenerateRoute,
      home: BlocListener<IntroCubit, IntroState>(
        listener: (context, state) {
          state.mapOrNull(
            userOnboarded: (state) => Navigator.pushReplacementNamed(
              context,
              AppRouter.main,
            ),
            userNew: (state) => Navigator.pushReplacementNamed(
              context,
              AppRouter.onboarding,
            ),
          );
        },
        child: const Scaffold(
          body: Center(child: CircularProgressIndicator()),
        ),
      ),
    );
  }
}
