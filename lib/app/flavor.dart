abstract class Flavor {
  String get title;
  String get apiBaseUrl;
  String get apiAccessKey;
}

class Test implements Flavor {
  @override
  String get title => 'Mediastack-Test';

  @override
  String get apiBaseUrl => 'http://api.mediastack.com/v1';

  @override
  String get apiAccessKey => 'edab7d1146ceebffac24cb072db70097';
}

class Prod implements Flavor {
  @override
  String get title => 'Mediastack';

  @override
  String get apiBaseUrl => 'http://api.mediastack.com/v1';

  @override
  String get apiAccessKey => 'edab7d1146ceebffac24cb072db70097';
}
