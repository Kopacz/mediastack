//ignore_for_file: long-method
import 'package:flutter/material.dart';
import 'package:mediastack/resources/dimensions.dart';
import 'package:mediastack/resources/palette.dart';
import 'package:mediastack/resources/styles.dart';

class Themes {
  ThemeData get appTheme => ThemeData(
        fontFamily: Styles.fontInter,
        useMaterial3: true,
        colorScheme: Palette.light,
        scaffoldBackgroundColor: Palette.light.background,
        appBarTheme: const AppBarTheme().copyWith(
          backgroundColor: Palette.light.background,
          foregroundColor: Palette.light.onBackground,
          titleTextStyle: Styles.interThin16.copyWith(
            color: Palette.light.onBackground,
          ),
        ),
        bottomNavigationBarTheme: BottomNavigationBarThemeData(
          backgroundColor: Palette.light.background,
        ),
        primaryTextTheme: const TextTheme(
          displayLarge: Styles.interThin20,
          displayMedium: Styles.interThin16,
          displaySmall: Styles.interThin12,

          headlineLarge: Styles.interRegular20,
          headlineMedium: Styles.interRegular16,
          headlineSmall: Styles.interRegular12,

          titleLarge: Styles.interMedium20,
          titleMedium: Styles.interMedium16,
          titleSmall: Styles.interMedium12,

          bodyLarge: Styles.interRegular16,
          bodyMedium: Styles.interRegular12,
          bodySmall: Styles.interRegular10,

          labelLarge: Styles.interSemiBold16,
          labelMedium: Styles.interSemiBold12,
          labelSmall: Styles.interThin10,
        ),
        textTheme: const TextTheme(
          displayLarge: Styles.interThin20,
          displayMedium: Styles.interThin16,
          displaySmall: Styles.interThin12,

          headlineLarge: Styles.interRegular20,
          headlineMedium: Styles.interRegular16,
          headlineSmall: Styles.interRegular12,

          titleLarge: Styles.interMedium20,
          titleMedium: Styles.interMedium16,
          titleSmall: Styles.interMedium12,

          bodyLarge: Styles.interRegular16,
          bodyMedium: Styles.interRegular12,
          bodySmall: Styles.interRegular10,

          labelLarge: Styles.interSemiBold16,
          labelMedium: Styles.interSemiBold12,
          labelSmall: Styles.interThin10,
        ),
        inputDecorationTheme: InputDecorationTheme(
          border: Styles.outlineInputBorder,
          focusedBorder: Styles.outlineInputBorder,
          enabledBorder: Styles.outlineInputBorder,
          disabledBorder: Styles.outlineInputBorder,
          errorBorder: Styles.errorOutlineInputBorder,
          focusedErrorBorder: Styles.errorOutlineInputBorder,
          filled: true,
          fillColor: Palette.light.background,
          hintStyle: Styles.interRegular12.copyWith(
            color: Palette.light.onSurfaceVariant,
          ),
          errorStyle: Styles.interRegular12.copyWith(
            color: Palette.light.error,
          ),
        ),
        bottomSheetTheme: const BottomSheetThemeData(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(Dimensions.radius16),
              topRight: Radius.circular(Dimensions.radius16),
            ),
          ),
        ),
        progressIndicatorTheme: ProgressIndicatorThemeData(
          color: Palette.light.primary,
        ),
        dividerTheme: const DividerThemeData(
          thickness: 1,
          space: Dimensions.spacing32,
        ),
        snackBarTheme: SnackBarThemeData(
          backgroundColor: Palette.light.background,
          behavior: SnackBarBehavior.floating,
          shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(Dimensions.radius4)),
          ),
        ),
      );
}
