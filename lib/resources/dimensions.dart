class Dimensions {
  Dimensions._();

  // Spacing
  static const spacing12 = 12.0;
  static const spacing16 = 16.0;
  static const spacing24 = 24.0;
  static const spacing32 = 32.0;

  // Radius
  static const radius4 = 4.0;
  static const radius8 = 8.0;
  static const radius16 = 16.0;

  // Stroke
  static const strokeWidth2 = 2.0;

  // Icons
  static const iconSize20 = 20.0;
  static const iconSize36 = 36.0;

  // Navigation Bar
  static const bottomNavBarHeight = 64.0;
  static const bottomNavBarElevation8 = 8.0;
  static const bottomNavBarItemHeight46 = 46.0;
  static const bottomNavBarItemWidth72 = 72.0;
  static const bottomNavBarItemBorderRadius76 = 76.0;
}
