class Labels {
  Labels._();

  // General
  static const errorLabel = 'Oops! Something went wrong.';

  // Home
  static const homeTitleLabel = 'MEDIASTACK';
  static const homeDefaultCategoryLabel = 'GENERAL';

  // Favourites
  static const favouritesTitleLabel = 'FAVOURITES';
}
