class Assets {
  Assets._();

  static const iconCategories = '${_pathIcons}ic_categories.svg';
  static const iconFavourites = '${_pathIcons}ic_favourites.svg';
  static const iconFavouritesFilled = '${_pathIcons}ic_favourites_filled.svg';
  static const iconHome = '${_pathIcons}ic_home.svg';
  static const iconHomeFilled = '${_pathIcons}ic_home_filled.svg';
  static const iconSearch= '${_pathIcons}ic_search.svg';

  static const imageError = '${_pathImages}img_error.png';

  static const _pathIcons = 'lib/assets/icons/';
  static const _pathImages= 'lib/assets/images/';
}
