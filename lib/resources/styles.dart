import 'package:flutter/material.dart';
import 'package:mediastack/resources/dimensions.dart';
import 'package:mediastack/resources/palette.dart';

class Styles {
  Styles._();

  static const fontInter = 'Inter';
  static const fontWorkSans = 'WorkSans';

  static const fontSize8 = 8.0;
  static const fontSize10 = 10.0;
  static const fontSize12 = 12.0;
  static const fontSize14 = 14.0;
  static const fontSize16 = 16.0;
  static const fontSize18 = 18.0;
  static const fontSize20 = 20.0;
  static const fontSize22 = 22.0;
  static const fontSize24 = 24.0;
  static const fontSize30 = 30.0;

  static const lineHeight10 = 10.0;
  static const lineHeight12 = 12.0;
  static const lineHeight16 = 16.0;
  static const lineHeight20 = 20.0;
  static const lineHeight24 = 24.0;
  static const lineHeight28 = 28.0;
  static const lineHeight32 = 32.0;

  static const interThin20 = TextStyle(
    fontFamily: fontInter,
    fontSize: fontSize20,
    height: lineHeight24 / fontSize20,
    fontWeight: FontWeight.w100,
    letterSpacing: 2.25,
  );

  static const interThin16 = TextStyle(
    fontFamily: fontInter,
    fontSize: fontSize16,
    height: lineHeight20 / fontSize16,
    fontWeight: FontWeight.w100,
    letterSpacing: 1.75,
  );

  static const interThin12 = TextStyle(
    fontFamily: fontInter,
    fontSize: fontSize12,
    height: lineHeight16 / fontSize12,
    fontWeight: FontWeight.w100,
    letterSpacing: 1.5,
  );

  static const interThin10 = TextStyle(
    fontFamily: fontInter,
    fontSize: fontSize10,
    height: lineHeight12 / fontSize10,
    fontWeight: FontWeight.w100,
    letterSpacing: 0.3,
  );

  static const interRegular20 = TextStyle(
    fontFamily: fontInter,
    fontSize: fontSize20,
    height: lineHeight24 / fontSize20,
    fontWeight: FontWeight.w400,
    letterSpacing: 0.3,
  );

  static const interRegular16 = TextStyle(
    fontFamily: fontInter,
    fontSize: fontSize16,
    height: lineHeight20 / fontSize16,
    fontWeight: FontWeight.w400,
    letterSpacing: 0.3,
  );

  static const interRegular12 = TextStyle(
    fontFamily: fontInter,
    fontSize: fontSize12,
    height: lineHeight16 / fontSize12,
    fontWeight: FontWeight.w400,
    letterSpacing: 0.3,
  );

  static const interRegular10 = TextStyle(
    fontFamily: fontInter,
    fontSize: fontSize10,
    height: lineHeight12 / fontSize10,
    fontWeight: FontWeight.w400,
    letterSpacing: 0.3,
  );

  static const interRegular8 = TextStyle(
    fontFamily: fontInter,
    fontSize: fontSize8,
    height: lineHeight10 / fontSize8,
    fontWeight: FontWeight.w400,
    letterSpacing: 0.3,
  );

  static const interMedium20 = TextStyle(
    fontFamily: fontInter,
    fontSize: fontSize20,
    height: lineHeight24 / fontSize20,
    fontWeight: FontWeight.w500,
    letterSpacing: 0.15,
  );

  static const interMedium16 = TextStyle(
    fontFamily: fontInter,
    fontSize: fontSize16,
    height: lineHeight20 / fontSize16,
    fontWeight: FontWeight.w500,
    letterSpacing: 0.15,
  );

  static const interMedium12 = TextStyle(
    fontFamily: fontInter,
    fontSize: fontSize12,
    height: lineHeight16 / fontSize12,
    fontWeight: FontWeight.w500,
    letterSpacing: 0.15,
  );

  static const interSemiBold16 = TextStyle(
    fontFamily: fontInter,
    fontSize: fontSize16,
    height: lineHeight20 / fontSize16,
    fontWeight: FontWeight.w600,
    letterSpacing: 0.1,
  );

  static const interSemiBold12 = TextStyle(
    fontFamily: fontInter,
    fontSize: fontSize12,
    height: lineHeight16 / fontSize12,
    fontWeight: FontWeight.w600,
    letterSpacing: 0.1,
  );

  static final outlineInputBorder = OutlineInputBorder(
    borderRadius: BorderRadius.circular(Dimensions.radius8),
    borderSide: BorderSide(color: Palette.light.outline),
  );

  static final errorOutlineInputBorder = OutlineInputBorder(
    borderRadius: BorderRadius.circular(Dimensions.radius8),
    borderSide: BorderSide(color: Palette.light.error),
  );
}
